/dts-v1/;
/plugin/;

/ {
	compatible = "brcm,bcm2835", "brcm,bcm2708", "brcm,bcm2709";
    fragment@0 {
        target = <&gpio>;
        __overlay__ {
            irq_gpio_11: irq_gpio_11_conf {
                brcm,pins = <11>; // GPIO 11
                brcm,function = <0>; // Input
                brcm,pull = <2>; // Pull up
            };
            irq_gpio_10: irq_gpio_10_conf {
                brcm,pins = <10>; // GPIO 10
                brcm,function = <0>; // Input
                brcm,pull = <2>; // Pull up
            };
            irq_gpio_0: irq_gpio_0_conf {
                brcm,pins = <0>; // GPIO 0
                brcm,function = <0>; // Input
                brcm,pull = <2>; // Pull up
            };
        };
    };

	fragment@1 {
		target-path = "/";
		__overlay__ {

			bridge: recalboxrgbjamma {
				compatible = "raspberrypi,recalboxrgbdual";
				#address-cells = <1>;
				#size-cells = <0>;

				vc4-vga666-mode = <6>;
				recalbox-rgb-jamma = <1>;
				ports {
					#address-cells = <1>;
					#size-cells = <0>;

					port@0 {
						reg = <0>;
						vga_bridge_in: endpoint {
							remote-endpoint = <&dpi_out>;
						};
					};

					port@1 {
						reg = <1>;
						vga_bridge_out: endpoint {
							remote-endpoint = <&vga_con_in>;
						};
					};
				};
			};

			vga {
				compatible = "vga-connector";
				port {
					vga_con_in: endpoint {
						remote-endpoint = <&vga_bridge_out>;
					};
				};
			};
		};
	};

	fragment@2 {
		target = <&dpi>;
		__overlay__  {
			pinctrl-names = "default";
			pinctrl-0 = <&vga666_mode6_pins>;
			status = "okay";
			port {
				dpi_out: endpoint@0 {
					remote-endpoint = <&vga_bridge_in>;
				};
			};
		};
	};

	fragment@4 {
		target = <&gpio>;
		__overlay__ {
			vga666_mode6_pins: vga666_mode6_pins {
				brcm,pins = <1
				      4  5  6  7  8  9
				      12 13 14 15 16 17
				      20 21 22 23 24 25>;
				brcm,function = <6>;
				brcm,pull = <0>;
			};
		};
	};

    fragment@5 {
        target = <&audio_pins>;
            __overlay__ {
                brcm,pins = < 18 19 >;
                brcm,function = < 2 >; /* alt5 alt5 */
                swap_lr;
            };
    };

    fragment@6 {
        target = <&chosen>;
        __overlay__  {
            bootargs = "snd_bcm2835.enable_headphones=1";
        };
    };

  fragment@8 {
    target = <&i2c1>;
    __overlay__ {
      status = "okay";
      pinctrl-names = "default";
      pinctrl-0 = <&i2c1_pins>;
      clock-frequency = <400000>;
      read-timeout-ms = <50>;
      jammactrl_0: jammactrl_0@25 {
        compatible = "raspberrypi,recalboxrgbjamma";
        reg = <0x25>;
        status = "okay";
        pinctrl-names = "default";
        pinctrl-0 = <&irq_gpio_11>;
        interrupt-parent = <&gpio>;
        interrupts = <11 2>;
        irq-gpios = <&gpio 11 0>;
      };
      jammactrl_1: jammactrl_1@24 {
        compatible = "raspberrypi,recalboxrgbjamma";
        reg = <0x24>;
        status = "okay";
        pinctrl-names = "default";
        pinctrl-0 = <&irq_gpio_10>;
        interrupt-parent = <&gpio>;
        interrupts = <10 2>;
        irq-gpios = <&gpio 10 0>;
      };
      jammactrl_2: jammactrl_2@27 {
        compatible = "raspberrypi,recalboxrgbjamma";
        reg = <0x27>;
        status = "okay";
        pinctrl-names = "default";
        pinctrl-0 = <&irq_gpio_0>;
        interrupt-parent = <&gpio>;
        interrupts = <0 2>;
        irq-gpios = <&gpio 0 0>;
      };
      jammaexpander: jammaexpander@26 {
        compatible = "raspberrypi,recalboxrgbjamma";
        reg = <0x26>;
        status = "okay";
      };
    };
  };

  fragment@9 {
		target = <&i2c1_pins>;
		__overlay__ {
			brcm,pins = <2 3>;
		};
	};

  fragment@10 {
    target-path = "/";
    __overlay__ {
      fan0: gpio-fan@0 {
        compatible = "gpio-fan";
        gpios = <&gpio 26 0>;
        gpio-fan,speed-map = <0 0>,<5000 1>;
        #cooling-cells = <2>;
      };
    };
  };

  fragment@11 {
    target = <&cpu_thermal>;
    polling-delay = <5000>;	/* milliseconds */
    __overlay__ {
      trips {
        cpu_hot: trip-point@0 {
          temperature = <60000>;	/* (millicelsius) Fan started at 60°C */
          hysteresis = <10000>;	    /* (millicelsius) Fan stopped at 50°C */
          type = "active";
        };
      };
      cooling-maps {
        map0 {
          trip = <&cpu_hot>;
          cooling-device = <&fan0 1 1>;
        };
      };
    };
  };
};
